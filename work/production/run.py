from cea import CEAProcess
import time
import sys
import json
from lamapi_cache import LamAPICacheWrapper
from pymongo import MongoClient


start = time.time()

line = " ".join(sys.argv[1:])
rows = json.loads(line)
objects_dump = open("/dump/objects.txt", "r")
literals_dump = open("/dump/literals.txt", "r")
lcw = LamAPICacheWrapper(objects_dump, literals_dump)
client = MongoClient('mongo', 27017, username='admin', password='DataMan2020!')
db = client.mantis
candidates_cache = db.candidates
cea = CEAProcess(rows, candidates_cache, lcw)

try:
    cea_results = cea.compute()
except Exception as e:
    client.mantis.logs.insert_one({"id_table": rows[0]["id_table"], "error": str(e)})
        
docs = []

for i, t in enumerate(rows):   
    docs.append({
        "id_table": t["id_table"],
        "row": t["row"],
        "cea": cea_results[i]
    })

client.mantis.cea.insert_many(docs)
objects_dump.close()
literals_dump.close()
client.close()

end = time.time()
print("Time:", end-start)

