class Cleaner:
    
    @classmethod
    def get_clean_text(cls, text):
        clean_text = text

        clean_text = clean_text.strip().lower()
        clean_text = cls.remove_extra_spaces(clean_text)

        # For elastic should escape
        # + - && || ! ( ) { } [ ] ^ " ~ * ? : \
        # TODO Remove json.dumps()
        #return json.dumps(clean_text)[1:-1]

        for ch in ['\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':']:
            if ch in clean_text:
                clean_text = clean_text.replace(ch, "\\" + ch)

        return clean_text
    
    @classmethod
    def remove_extra_spaces(cls, s):
        """
        Remove unnecessary spaces
        :param s:
        :return:
        """
        return " ".join(s.split()) 
        
        
