import json
from row import Row
from linkage import Linkage
import redis

targets_cache = redis.Redis(
            host='redis_mantis',
            port=6379,
            db=0
        )

class CEAProcess:
    def __init__(self, items: list, candidates_cache, LamAPICacheWrapper, my_targets=None):
        self._items = items
        self._candidates_cache = candidates_cache
        self._LamAPICacheWrapper = LamAPICacheWrapper
        self._my_targets = my_targets
        
    def compute(self):
        results = []
        for item in self._items:
            row = self._build_row(item["data"], item["id_table"])
            if row.get_subject_cell() is None:
                # TODO: This is a serious error. What should I do?
                print("WARNING: row has no subject column. Ignoring...")
                continue
                    
            linkage = Linkage(row, self._LamAPICacheWrapper)
            links = linkage.get_links()
            subjects, new_links = linkage.get_subjects(links)
            results.append(
                (subjects, new_links)
            )
        
        return results

    def _build_row(self, cells, id_table):
        row = Row()
        if self._my_targets is None:
            targets = json.loads(targets_cache.get(id_table))
        else:
            targets = self._my_targets[id_table]
            
        for i, cell in enumerate(cells):
            if i in targets["ne"]:
                candidates = self._get_candidates(cell)
                is_subject = i == targets["subj"]
                row.add_ne_cell(cell, candidates, is_subject)
            elif i in targets["lit"]:
                row.add_lit_cell(cell)
            else:    
                row.add_notag_cell(cell)
                
        return row       
   
    def _get_candidates(self, cell):
        result = self._candidates_cache.find_one({"cell": cell})
        if result is not None:
            candidates = result["candidates"]
        else:
            candidates = []
        return candidates
    