import json
from cleaner import Cleaner 

targets = {
    "test1": {
                "subj": 0,
                "ne": [0, 1, 2],
                "lit":[3, 4, 5]
             }
}


with open("/dump/candidates.index", "r") as f:
    candidates_index = json.loads(f.read())

candidates_cache = open("/dump/candidates.map", "r")


class Compute:
    def __init__(self, items: list): #normalized_map: dict, candidates_map: dict):
        self._items = items
        
    def compute(self):
        results = []
        for item in self._items:
            row = self._build_row(item["data"], item["id_table"])
            if row.get_subject_cell() is None:
                # TODO: This is a serious error. What should I do?
                print("WARNING: row has no subject column. Ignoring...")
                continue
                    
            #linkage = Linkage(row)
            #links = linkage.get_links()
            #subjects = linkage.get_subjects(links)
            results.append(
                row
            )
        
        return results

    def _build_row(self, cells, id_table):
        row = Row()
      
        for ne_cell in targets[id_table]["ne"]:
            content = cells[ne_cell]
            candidates = self._get_candidates(content)
            print(candidates)
            print(type(candidates))
            candidates = []
            is_subject = ne_cell == targets[id_table]["subj"]
            row.add_ne_cell(content, candidates, is_subject)
        for lit_cell in targets[id_table]["lit"]:
            content = cells[lit_cell]
            row.add_lit_cell(content)
        
        return row    
   
    def _get_candidates(self, cell):
        candidates_cache.seek(candidates_index[cell])
        print("LINE READ:", candidates_cache.readline())
        candidates = json.loads(candidates_cache.readline())
        return candidates[cell]    
   
  
class Cell:
    def __init__(self, content: str, candidates: list, is_lit_cell=False):
        self.content = content
        self.is_lit_cell = is_lit_cell

        self._cands_entities = set([
            cand["uri"]
            for cand in candidates
        ])
        self._cands_labels = {}
        for candidate in candidates:
            label = Cleaner.get_clean_text(candidate["label"]) 
            uri = candidate["uri"]
            if uri not in self._cands_labels:
                self._cands_labels[uri] = set()
            self._cands_labels[uri].add(label)

    def candidates_entities(self):
        return self._cands_entities

    def candidates_labels(self, entity):
        return self._cands_labels.get(entity, [])

        
class Row:
    def __init__(self):
        self.subject_cell = None
        self.cells = []

    def add_ne_cell(self, content: str, candidates: list, is_subject=False):
        cell = Cell(content, candidates)
        self.cells.append(cell)

        if is_subject:
            self.subject_cell = cell

    def add_lit_cell(self, content: str):
        cell = Cell(content, [], True)
        self.cells.append(cell)

    def get_subject_cell(self):
        return self.subject_cell

    def get_cells(self):
        return self.cells

    def __len__(self):
        return len(self.cells)  