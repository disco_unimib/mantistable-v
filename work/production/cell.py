from cleaner import Cleaner

class Cell:
    def __init__(self, content: str, candidates: list, is_lit_cell=False, is_notag_cell=False):
        self.content = content
        self.is_lit_cell = is_lit_cell
        self.is_notag_cell = is_notag_cell

        self._cands_entities = set([
            cand["uri"]
            for cand in candidates
        ])
        self._cands_labels = {}
        for candidate in candidates:
            label = Cleaner.get_clean_text(candidate["label"]) 
            uri = candidate["uri"]
            if uri not in self._cands_labels:
                self._cands_labels[uri] = set()
            self._cands_labels[uri].add(label)

    def candidates_entities(self):
        return self._cands_entities

    def candidates_labels(self, entity):
        return self._cands_labels.get(entity, [])