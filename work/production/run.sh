#!/bin/bash
start=`date +%s`
file="missings_rows.jsonl"
num_procs=$1
num_jobs="\j" 
total_blocks=40

for value in $(seq $total_blocks); do
  line="${value}p"
  rows=$(sed -n $line $file) 
  while (( ${num_jobs@P} >= num_procs )); do
    wait -n
  done
  echo "Process Block: ${value}"
  python3 run.py $rows &
done
wait

end=`date +%s`
runtime=$((end-start))
echo $runtime
