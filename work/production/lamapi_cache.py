import json
import math
import redis

objects_index = redis.Redis(
            host='redis_mantis',
            port=6379,
            db=1
        )

literals_index = redis.Redis(
            host='redis_mantis',
            port=6379,
            db=2
        )    

class LamAPICacheWrapper:
    BLOCK_FACTOR = 100
    def __init__(self, objects_dump, literals_dump): 
        self._objects_dump = objects_dump
        self._literals_dump = literals_dump
        
    def get_objects(self, candidates):
        return self._get_data(candidates, self._objects_dump, objects_index)
    
    def get_literals(self, candidates):
        return self._get_data(candidates, self._literals_dump, literals_index)
    
    def _get_data(self, candidates, dump_file, index_file):
        blocks = {}  # group by block in order to reduce stream from disk
        for candidate in candidates:
            try:
                index = int(candidate[1:])
                index_block = str(math.ceil(index / self.BLOCK_FACTOR))
                if index_block not in blocks:
                    blocks[index_block] = []
                blocks[index_block].append(candidate)
            except Exception as e: 
                print("Error", e)
        
        results = {}
        for index_block in blocks:
            dump_index_block = int(index_file.get(index_block))
            if dump_index_block is None:
                continue
            dump_file.seek(dump_index_block)    
            block = json.loads(dump_file.readline())
            #block = self._read_from_dump(dump_file, dump_index_block)    
            for candidate in blocks[index_block]:
                results[candidate] = block["v"][candidate]    
                
        return results 
    