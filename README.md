# Citing

[1] Avogadro, R., & Cremaschi, M. (2021). MantisTable V: A novel and efficient approach to Semantic Table Interpretation. In SemTab@ ISWC (pp. 79-91).

Checkout s-Elbat [https://bitbucket.org/disco_unimib/selbat](https://bitbucket.org/disco_unimib/selbat)

# MantisTable #
MantisTable V approach, it needs also the API available at [https://bitbucket.org/disco_unimib/mantistable-v-api](https://bitbucket.org/disco_unimib/mantistable-v-api)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details